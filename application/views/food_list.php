<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border-bottom: 1px solid #ddd;
            padding: 15px;
            text-align: left;
        }
        th {
            background-color: #EE77BF;
            color: white;
        }
        tr:hover {background-color: #f5f5f5;}
    </style>

<table>
    <tr>
        <th>Food id</th>
        <th>Food name</th>
        <th>Food Prices</th>
        <th>Food Amount</th>
    </tr>
    <?php
        foreach($query as $row) {
    ?>
    <tr>
            <td><?php echo $row['fid']?></td>
            <td><?php echo $row['fname']?></td>
            <td><?php echo $row['fprice']?></td>
            <td><?php echo $row['famount']?></td>
    </tr>
    <?php
        }
    ?>
</table>

</body>
</html>